package com.grid.gridcomp.service;

import com.grid.gridcomp.mapper.GridMapper;
import com.grid.gridcomp.vo.BoardVO;
import com.grid.gridcomp.vo.GridVO;
import com.grid.gridcomp.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GridService {

    @Autowired
    GridMapper mapper;

    //dataTables 사용시 쓰는 서비스
    public Map<String, Object> getGrid(Map<String, Object> param) throws Exception {
        Map<String, Object> resultMap = new HashMap<>();
        int total = mapper.getCount(param);
        List<BoardVO> list = mapper.getGrid(param);

        resultMap.put("total", total);
        resultMap.put("content", list);

        return  resultMap;
    }

    // 전체 리스트 개수 가져오기
    public int totalGrid(Map<String, Object> param) throws Exception {
        return  mapper.getCount(param);
    }


    //일반 페이징처리시 사용하는 서비스
    public List<BoardVO> getTableData(SearchVO searchVO) throws  Exception {
        return  mapper.getTableData(searchVO);
    }


    //JQ그리드 컴포넌트 처리시 사용하는 서비스
    public  List<BoardVO> getJqGrid(Map<String, Object> param) throws Exception {
        return  mapper.getGrid(param);
    }
}
