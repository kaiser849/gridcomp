package com.grid.gridcomp.controller;

import com.grid.gridcomp.service.GridService;
import com.grid.gridcomp.vo.BoardVO;
import com.grid.gridcomp.vo.GridVO;
import com.grid.gridcomp.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/test")
public class TestController {


    private GridService service;

    @Autowired
    public void setGridService(GridService service) {
        this.service = service;
    }

    @RequestMapping("/data/table")
    public String view() {
        return "views/grid";
    }

    @RequestMapping("/bqa/list")
    @ResponseBody
    public Map<String, Object> grid(SearchVO vo){


        Map<String, Object> resultMap = new HashMap<>();

        try{
            Map<String, Object> param = new HashMap<>();

            param.put("start", vo.getStart());
            param.put("end", vo.getLength());
            param.put("sidx", vo.getSidx());
            param.put("sord", vo.getSord());

            resultMap = service.getGrid(param);

        }catch (Exception e) {
            e.printStackTrace();
        }

        return resultMap;
    }



    @RequestMapping("/jqgrid")
    public String jqgridView() {
        return "views/jqgrid";
    }

    @RequestMapping(value="/bqa/jqgrid", method = RequestMethod.POST , produces = {"application/json"})
    @ResponseBody
    public Map<String, Object> jqgrid(@RequestParam Map<String, Object> formData){

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        Map<String, Object> resultMap = new HashMap<>();

        try{

            if (formData.containsKey("rows") && formData.get("rows") != null) {
                rows = Integer.parseInt(formData.get("rows").toString());
            } else {
                rows = 30;
            }

            if (formData.containsKey("page") && formData.get("page") != null) {
                page = Integer.parseInt(formData.get("page").toString());
            } else {
                page = 1;
            }

            startNum = rows * (page - 1);

            int totalCount = service.totalGrid(formData);

            formData.put("start", startNum);
            formData.put("end", rows);

            List<BoardVO> list = service.getJqGrid(formData);

            if (list.size() > 0) {
                total_pages = (int) Math.ceil((double) totalCount / rows);
            } else {
                total_pages = 0;
            }

            resultMap.put("total", total_pages);    // the total pages of the query
            resultMap.put("records", totalCount);     // the total records from the query
            resultMap.put("rows", list);

        }catch (Exception e) {
            e.printStackTrace();
        }

        return resultMap;
    }

    @RequestMapping("/chart")
    public String chart() {
        return "views/chart";
    }

}
