package com.grid.gridcomp.controller;

import com.grid.gridcomp.service.ChartService;
import com.grid.gridcomp.vo.ChartDataVO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/chart")
public class ChartController {

    private final ChartService service;


    @RequestMapping(value = "/chartData")
    public Map<String, Object> getChartData(@RequestParam Map<String, Object> param) {

        Map<String, Object> result = new HashMap<>();
        List<ChartDataVO> chartDataList = null;

        try {

            chartDataList = service.getChartDataList(param);


            if (chartDataList == null) {
                result.put("resultCode", 300);
                result.put("resultMsg", "No chartData");

            } else {
                result.put("resultCode", 200);
                result.put("resultMsg", "No chartData");
            }

            result.put("chartData", chartDataList);

        } catch (Exception e) {
            e.printStackTrace();
            result.put("resultCode", 9999);
            result.put("resultMsg", "Fail get chartData");
        }

        return result;
    }

}
