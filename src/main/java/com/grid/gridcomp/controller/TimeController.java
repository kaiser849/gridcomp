package com.grid.gridcomp.controller;


import com.grid.gridcomp.vo.TestSearchVO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@RestController
public class TimeController {


    @PostMapping("/my/time")
    public void get(@RequestBody TestSearchVO.CreateRequest vo) {
        ZoneId utcZone = ZoneId.of("UTC");

        ZonedDateTime utcDateTime = vo.getStartDate().atZone(utcZone);

        ZonedDateTime zdt = utcDateTime.withZoneSameInstant(ZoneId.of("Asia/Seoul"));
        String date = zdt.format(DateTimeFormatter.ISO_LOCAL_DATE);
        System.out.println(zdt);
        System.out.println(date);
    }

}
