package com.grid.gridcomp.controller;

import com.grid.gridcomp.service.GridService;
import com.grid.gridcomp.vo.BoardVO;
import com.grid.gridcomp.vo.PagingVO;
import com.grid.gridcomp.vo.SearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/test")
public class BoardController {

    private GridService service;

    @Autowired
    public void setGridService(GridService service) {
        this.service = service;
    }

    @RequestMapping("/board")
    public ModelAndView view(SearchVO searchVO) {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = new HashMap<>();
        List<BoardVO> list = null;


        view.setViewName("views/table_grid");

        try {

            //전체 레코드 개수를 가져온다.
            int totalCount = service.totalGrid(param);

            //페이징처리 객체를 불러온다.
            PagingVO pagingVO = new PagingVO();
            //페이징객체에 전체 카운트 저장
            pagingVO.setTotalCount(totalCount);
            //페이징객체에 html에서 넘어온 이동할 페이지를 저장한다
            pagingVO.setCurrentPage(searchVO.getCurrentPage() );

            //페이징 객체에서 계산된 불러올 리스트의 시작위치를 검색 객체에 넣는다.
            searchVO.setStartRow( pagingVO.getStartRow() );
            //페이징 객체에서 계산된 불러올 리스트의  마지막위치를  검색 객체에 넣는다.
            searchVO.setLastRow( pagingVO.getCountPerPage());

            // 테이블에 표현할 리스트를 가져온다.
            list = service.getTableData(searchVO);

            //나중에 검색어같은게 있으면 유지해줘야하니까 검색객체 자체를 그냥 html 로 보낸다.
            view.addObject("currentPage", searchVO);
            //페이징 정보를 보낸다.
            view.addObject("pager", pagingVO);
            //페이징 객체에서 그려진 html 구문을 보낸다. 해당 구문은 페이지를 표지할 html 이 된다.
            view.addObject("paging", pagingVO.getPager());
            // 테이블에 표현할 리스트를 보낸다
            view.addObject("boardList", list);

            //리스트가 있을 경우는 전체카운트를 보내고 없으면 그냥 0으로 준다.
            if(list != null) {
                view.addObject("listSize", totalCount);
            }else{
                view.addObject("listSize", 0);
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

        return view;

    }

}
