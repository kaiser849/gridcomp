package com.grid.gridcomp.mapper;

import com.grid.gridcomp.vo.ChartDataVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ChartMapper {
    public List<ChartDataVO> getChartDataList(Map<String, Object> param) throws Exception;
}
