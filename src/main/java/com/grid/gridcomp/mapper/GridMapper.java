package com.grid.gridcomp.mapper;

import com.grid.gridcomp.vo.BoardVO;
import com.grid.gridcomp.vo.GridVO;
import com.grid.gridcomp.vo.SearchVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface GridMapper {

    int getCount(Map<String, Object> param) throws  Exception;
    List<BoardVO> getGrid(Map<String, Object> param) throws  Exception;
    List<BoardVO> getTableData(SearchVO searchVO) throws  Exception;
}
