package com.grid.gridcomp.vo;

import lombok.Data;

@Data
public class GridVO {

    private int utterId;
    private String sessionId;
    private int botId;
    private String userId;
    private int dialogSeq;
    private String speaker;
    private String sentence;
    private String contentJson;
    private String domainName;
}
