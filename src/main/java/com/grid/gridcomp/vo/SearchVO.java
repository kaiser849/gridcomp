package com.grid.gridcomp.vo;

import lombok.Data;

@Data
public class SearchVO {

    private int start;   // grid 컴포넌트 사용시 시작 위치
    private int length;  // grid 컴포넌트 사용 시 종료 위치
    private String sidx;    // grid 에서 사용하는 order by 대상 컬럼
    private String sord;    // grid 에서 사용하는 order by 순서
    private int currentPage;    // 일반 페이징 시 필요한 이동할 페이지
    private int startRow;          //일반 페이지 시작 게시물 번호
    private int lastRow;           //일반 페이지 마지막 게시물 번호
}
