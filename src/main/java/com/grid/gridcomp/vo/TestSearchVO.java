package com.grid.gridcomp.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDateTime;


public class TestSearchVO {

    @Getter
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor
    @Builder
    public static class CreateRequest {

        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
        private LocalDateTime startDate;
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
        private LocalDateTime endDate;

    }
}
