package com.grid.gridcomp.vo;

import lombok.Data;

@Data
public class ChartDataVO {
    private String x;
    private double y;
}
