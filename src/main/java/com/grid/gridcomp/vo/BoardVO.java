package com.grid.gridcomp.vo;

import lombok.Data;

@Data
public class BoardVO {

    private int boardId;
    private String boardTitle;
    private String boardContent;
    private String boardAuthor;
    private int readCnt;
    private String regDate;

}
