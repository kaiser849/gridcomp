package com.grid.gridcomp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GridcompApplication {

    public static void main(String[] args) {
        SpringApplication.run(GridcompApplication.class, args);
    }

}
